# Deployments and Replica Sets

Navigate to the folder `kubernetesexercises/deployments-rolling-updates`.

### `create` Deployments

Let's start by creating a deployment. Usually, you will use the yaml files called _manifest_, we will experiment with that later.
But knowing how to use imperative `kubectl` commands can come in quite handy.

```bash
kubectl create deployment hello --image=gcr.io/google-samples/hello-app@sha256:2b0febe1b9bd01739999853380b1a939e8102fd0dc5e2ff1fc6892c4557d52b9
```

Let's look at what happened:
```
kubectl get pods -w
```

> **Note**: You can exit `--watch/-w` with `ctrl + c`

---

But of course not only a pod was created:

```
kubectl get deployment,replicaset,pod 
```

You can use the `describe` command to describe every object in detail and most importantly view the related events (**Note how the pod name is generated randomly**):

```
kubectl describe pod hello
```

### Finally remove the deployment again
```
kubectl delete deploy hello
```

> **Note**: The pod and replicaset that was created was also deleted.

## Creating a deployment using declarative configuration files

To create one or more objects that are declared in a file, just run

```
kubectl apply -f echo-simple-deployment.yaml
```

Let's look at what was created

```
kubectl get deploy,pods,replicaset
```

---

Now let's talk to our echo service, by port-forwarding _in a separate terminal window_:

```
kubectl port-forward deployment/echo 8080:8080
```

> **Note**: Make sure to exit `kubectl port-forward` using `CTRL+C` after finishing

---

Is somebody out there?

```
curl localhost:8080
```

Now change the value of `replicas: 1` to `replicas: 2` and apply the file again:
```
k get deploy,pods,replicaset
```

Lets observe what happens, if we kill one of the pods:

```
kubectl delete pod echo-<ID>
```

See how the replicaset made sure that another pod was spawned automatically:
```
kubectl get pods
```

You can also scale up or down a deployment using kubectl
```bash
kubectl scale deploy echo  --replicas=4
```

```
kubectl get deploy
```

Scaling down to zero is possible too:
```
kubectl scale deploy echo  --replicas=0
```
```
kubectl get pods
```

# Rolling Updates
### `create` Deployment

Create a deployment with multiple replicas
```
kubectl create deployment nginx --image=nginx:latest
kubectl expose deployment nginx --port 80 --type LoadBalancer
kubectl scale deployment nginx --replicas=4
```

---
>**Note**: We will talk about services in detail later. For now knowing that services load balance to multiple pods is enough
---

Note down the loadbalancer IP from the service we just generated (this can take a while)
```
kubectl get service
```

## Watch the server's version and pods
Open another terminal and paste the following command to continously query the image version:
```bash
while true; do date && curl -m 3 -sI  <EXTERNAL-IP>  | grep Server; sleep 1; done
```
You can  continuously watch the pods with the `-w` flag or with
```bash
watch -n 1 kubectl get pods,rs
```
---
>**Note**: You can exit `watch` with `ctrl + c`
---

## Update the deployment

Now change the deployment's image version:
```bash
kubectl set image deployment/nginx nginx=nginx:1.21.5
```
---

Watch how the pods are being replaced one by one without causing any outage.

You can watch the rollout status using:
```
kubectl rollout status deployment nginx
```
The rollout history is accessible using:

```
kubectl rollout history deployment nginx
```

We can inspect the revision in more detail with
```bash
kubectl rollout history deploy/nginx --revision 2
```

Next, let's try out what happens in case of a failing deployment.

```
kubectl set image deployment/nginx nginx=nginx:na
```

We can undo this using:
```
kubectl rollout undo deployment nginx
```
## Export a resource to yaml
Save the current state of the deployment into a yaml file to make editing easier. 
```bash
kubectl get deployment nginx -o yaml > deployment.yaml
```

## StrategyType: Recreate
Change the field 
```
 strategy:
    rollingUpdate:
      maxSurge: 25%
      maxUnavailable: 25%
    type: RollingUpdate
```
to
```
 strategy:
    type: Recreate
```

Now reapply the failed update and observe what happens:

```bash
kubectl replace -f deployment.yaml
```

> **Note**: The Strategy `Recreate` can be useful in debug environments, but is not recommended otherwise.

---

## Replicasets

Do you remember how Deployments perform rolling updates?
Lets check the ReplicaSets that have been created in the meantime.

```bash
kubectl get replicaset
```
