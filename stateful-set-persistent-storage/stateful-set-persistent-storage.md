Navigate to the folder `kubernetesexercises/stateful-set-persistent-storage`.

# EmptyDir

Let's take a look at the deployment defined in `deployment-emptyDir.yaml`. 
At the very bottom, it describes a volume as an ephemeral `emptyDir`. Both the `initContainer` as well as the main container mount this volume.

What do these containers do?

<details><summary>Click here for the answer</summary>
<p>
The initContainer creates an empty file inside the emptyDir /cache/.
The main container prints out /cache/ using the ls command before creating a file, too, and exiting.
</p>
</details>

Let's put a watcher on the pods
```bash
kubectl get pods -w
```

And then apply the deployment in another terminal window using 
```bash
kubectl apply -f deployment-emptyDir.yaml
```

1) Can you find out why the pod is in a crash loop using `kubectl logs deploy/empty-dir-demo`?

2) Why does the output grow with each iteration of the crash loop?

<details><summary>Click here for the answer</summary>
<p>
After the deployment is started, the initContainer starts, creates a file in /cache/ and then exits. Then, the main container starts, prints out the content of the /cache/ directory, creates a new file and exits deliberately.
Because the deployment was tasked to have 1 replica running, it regards the exited main container as crashed and restarts it. 

This also explains, why the number of files in /cache/ increases with each crash loop: the main container starts again, prints out the /cache/ directory, creates another file, exits and gets started again.
</p>
</details>

## Cleanup
```
kubectl delete deploy/empty-dir-demo
```

# PersistentVolumes

Let's list the available storage classes using

```bash
kubectl get storageclass
```

The storage classes available may differ depending on the cloud provider that manages your k8s cluster.

## Creating a PersistentVolume (PV)

Usually, PVs are created indirectly using a PersistentVolumeClaim (PVC). Take a look at `pvc-test.yaml`. Then, apply it:
```bash
kubectl apply -f pvc-test.yaml
```

Inspect the created PVC:
```bash
kubectl get pvc
```

Why is it `Pending`? You can find out using `kubectl describe`.

<details><summary>Click here for the answer</summary>
<p>
At this point, the PVC has not been bound to a consumer. Therefore, there is also no PV, yet.
</p>
</details>
---

Take a look at `deployment-pvc.yaml` and then apply it to create a consumer:
```bash
kubectl apply -f deployment-pvc.yaml
```

Look at the PVC again:
```bash
kubectl get pvc
```

As you can see, it is now `bound`. Looking at the persistent volumes we can also see that one has been created:
```bash
kubectl get pv
```

1) What do you think will happen if you delete the PVC?
2) What do you think will happen if you delete the consumer (the deployment)?

<details><summary>Click here for the answer</summary>
<p>
1) k8s will fail to terminate the PVC, because it is still bound to your active deployment. It will be stuck terminating it.

2) Now that the consumer is deleted, the PVC can also be deleted. Note, the PV will also be deleted because by default it had a reclaim policy of "Delete".
</p>
</details>

## Reclaim Policies

Let's make sure our data does not get lost after we remove the associated PVC. If you deleted our test PVC and its consumer in the last exercise make sure to recreate it:
```bash
kubectl apply -f pvc-test.yaml
```
```bash
kubectl apply -f deployment-pvc.yaml
```

Now, let's change the `persistentVolumeReclaimPolicy` of the provisioned PV to `Retain` using the following command:

```bash
kubectl patch pv $(kubectl get pvc pvc-test -o jsonpath="{.spec.volumeName}") -p '{"spec":{"persistentVolumeReclaimPolicy":"Retain"}}'
```

Verify that the `ReclaimPolicy` is now set to `Retain`:
```bash
kubectl get pv
```

Let's delete the PVC and it's consumer again:
```bash
kubectl delete -f deployment-pvc.yaml
kubectl delete -f ./pvc-test.yaml
```

Now, check the list of PVs again:
```bash
kubectl get pv
```

The PV is now `Released` but not deleted.
Let's see if the PV will be associated with the PVC again, when we create it once more.
```bash
kubectl apply -f pvc-test.yaml
kubectl apply -f deployment-pvc.yaml
```

Unfortunately, a new PV with reclaim policy `Delete` was created alongside our old PV and bound to our newly provisioned deployment. Can you figure out why? 
Hint: try inspecting the YAML of your old PV with `kubectl get pv <YOUR-PV-NAME> -o yaml`

<details><summary>Click here for the answer</summary>
<p>
The claimRef field of our old PV still references the PVC that claimed it earlier. Setting this field to "null" releases the PV so it can be reclaimed again. You can do so with the following command:

kubectl patch pv <YOUR-PV-NAME> -p '{"spec":{"claimRef": null}}'
</p>
</details>


# Stateful Set

The required manual effort of handling PVCs and PVs helps to understand the need for `StatefulSet`.

Before we deploy a StatefulSet, start watching the pods in your namespace in another terminal:
```bash
kubectl get pods -w
```

Now, deploy the StatefulSet we prepared for you:
```bash
kubectl apply -f stateful-set.yaml
```

Observe how a `PersistentVolume` and a `PersistenVolumeClaim` where created implicitly:
```bash
kubectl get pvc
kubectl get pv
```
Let's try out this demo application. Activate port-forwarding to be able to `curl` it:
```bash
kubectl port-forward svc/nginx 8080:80
```

In a new terminal, try:
```bash
curl localhost:8080
```

The result is unsatisfactory, so let's create a `index.html` with the following command:
```bash
kubectl get pods -o name | grep web| while read -r pod; do kubectl exec $pod -- /bin/bash -c "echo 'Enjoy your life in full trains.' > /usr/share/nginx/html/index.html"; done
```

Query the server again:
```bash
curl localhost:8080
```

Looks better, doesn't it?
Now, let's scale the stateful set back to 0 replicas, wait a bit and scale it back up again (keep watching what happens with `kubectl get pods -w`):

```bash
kubectl scale statefulset web --replicas 0
kubectl scale sts web --replicas 3
```

Don't forget to activate port-forwarding again!
Do you think our new `index.html` persisted the temporary non-existence of consuming pods?
```bash
curl localhost:8080
```

Observe `pv`s and `pvcs` again
```bash
kubectl get pvc
kubectl get pv 
```

As you can see from the timestamps, both have persisted.

# Bonus Exercise: Cronjob

Delete the deployment we created with `kubectl delete -f deployment-pvc.yaml`, since the cronjob example we will try out next uses the same pvc.
Start by deploying the cronjob:

```bash
kubectl apply -f cronjob-pvc.yaml
```
Watch the pods and jobs in another shell (end the background processes later with `killall kubectl`)

```bash
kubectl get pods -w & kubectl get jobs -w
```

Now, try to modify the cronjob to only run on the 11th of November at 11:11:11.

