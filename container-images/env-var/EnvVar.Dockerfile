FROM docker.io/alpine

#ENV ENV_VAR "Value"

ENTRYPOINT if [ -z ${ENV_VAR+x} ]; then echo "ENV_VAR is not set"; false; else echo ${ENV_VAR}; fi
