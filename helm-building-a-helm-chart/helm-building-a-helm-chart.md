# Helm (2/2) Building you a helm chart

Navigate to the folder `kubernetesexercises/helm-building-a-helm-chart`.

In this exercise, we will try to build a helm chart for a prebuilt spring boot application serving a static site.

**Note:** This is one if not perhaps the most challenging exercise in this workshop. Not all necessary steps will be explained in detail, so you may have to use the techniques and commands we learned in previous exercises. Don't feel bad if you don't finish.

## Useful helm commands for creating and debugging helm charts

|  helm command | description   |
|---|---|
|  `helm create [name]` |  Create a directory containing the common chart files and directories (`chart.yaml`, `values.yaml`, `charts/` and `templates/`): |
|  `helm package [chart-path]` |  Package a chart into a chart archive |
|  `helm lint [chart] ` |  Run tests to examine a chart and identify possible issues |
|  `helm show all [chart]` |   Inspect a chart and list its default values |
|  `helm show values [chart] ` | Display the chart’s values |
|  `helm template [release] [chart] (-f/--values [values-file]) (-s/--show-only [template file]` | Render one or all manifests of a helm chart (e.g.: `helm template test . -f values-edited.yaml -s templates/ingress.yaml`)  |
|  `helm upgrade [release] [chart] --install (-n/--namespace [namespace])` | Upgrade a release. If it does not exist on the system, install it |
|  `helm get manifest [release]` |  display the manifest for a named release currently deployed |
|  `helm get values [release] ` |   display the values used to deploy a release currently deployed |


## Creating a new helm chart from scratch

To create a helm chart, we don't have to start from scratch or copy from other sources. Helm comes packaged with a helm template.
Since we are going to deploy a container image containing spring boot, `spring-demo-chart` seems to be a fitting name.
To generate a helm chart based on this template, type:

```bash
helm create spring-demo-chart
```
Helm will create a new directory `spring-demo-chart` containing an amount of files that may seem frightening at first.

```
spring-demo-chart/
├── Chart.yaml
├── charts
├── templates
│   ├── NOTES.txt
│   ├── _helpers.tpl
│   ├── deployment.yaml
│   ├── hpa.yaml
│   ├── ingress.yaml
│   ├── service.yaml
│   ├── serviceaccount.yaml
│   └── tests
│       └── test-connection.yaml
└── values.yaml
```

## Exploring and deploying a local helm chart

Try out the command `helm show values` with your current helm chart. This command will output the contents of `values.yaml`

```
helm show values .
```
**Note:** This command is useful mainly for helm charts from remote repositories, for example: `helm show values bitnami/nginx`

You will note some mentions of some known concepts but also of some advanced concepts like `securityContext` or `nodeAffinity`. You can safely ignore those at the moment.


As we did before, we will start by deploying the sample helm chart as is...

```
helm upgrade --install spring-demo-release ./spring-demo-chart
```

After a successful deployment some information and instructions will be shown. Please keep the output in mind when you explore the helm release, since we will soon begin with influencing this output.

Confirm your release was successful:

```
helm ls
```

### Exercise 1
  1. Something else apart from our well known basic building blocks `deployment` and `service` was deployed. Can you find out what using `helm get` commands?
  2. What is the origin of `helm status`? 
     1. `REVISION`
     2. `NOTES`
  3. Edit your chart in `NOTES.txt`, so it contains at least your name. You can safely ignore all those curly braces. 
  4. The output of `helm ls` shows a *chart version* and *app-version*. Change those versions so that `chart version` is `0.2.0` and `app-version` is `1.17.0`
  5. Edit the ImagePullPolicy to `Always` in `values.yaml`. This is often regarded as a best practice.   
  5. Deploy a new revision of your chart and confirm your changes where successful
   
### Bonus Exercise 1
 1. Use the go templating mechanism to insert your name from the `values.yaml` in your chart. To do this, add a value field and include it in `NOTES.txt` using `{{ .Values }}`
 2. Add `bitnami/redis` as a chart dependency:
  ```
  # Chart yaml 
  dependencies:
  - name: redis
    condition: redis.enabled
    version: "17.9.5"
    repository: "https://charts.bitnami.com/bitnami"
  ```
 3. You will notice that updating again will result in an error. Fetch the dependent chart using `helm dependency update`
 4. Disable the redis chart default by adding the value `redis.enabled: false` *(in separate lines)*
   
---

## Modifying the chart template

The container image we currently deploy is `nginx:latest`. This is obviously not spring boot. 

Create a values file for your custom values referencing another image. Think about separating default values from environment or release specific values.

```shell
cat <<EOT >> custom-values.yaml
image:
  repository: registry.gitlab.com/mt-ag-k8s-training-course/kubernetesexercises/spring-boot-demo
  pullPolicy: Always
  tag: "2.0"
EOT
```

Alternatively, you could try using the image you built during the `gitlab-ci` exercise.

Now update your helm release
```
helm upgrade --install spring-demo-release ./spring-demo-chart -f custom-values.yaml
```

You can find the user supplied values again with a command that is very useful for debugging helm releases
```shell
helm get values spring-demo-release
```


### Exercise 1

1. The `release` doesn't seem to have been deployed correctly. Try to diagnose the problem using `kubectl logs`, `kubectl describe` and `kubectl get`. Be especially mindful of ports. Why is the pod getting restarted?

**Note**

[Troubleshooting kubernetes](https://learnk8s.io/troubleshooting-deployments) is a flowchart providing a good overview about how to troubleshoot errors like this.


2. Edit the helm chart using the insight you gained. Deploy your changes.

<details><summary>spoiler - solution</summary>
<p>

```yaml
   # templates/deployment.yaml
      containers:
        - name: {{ .Chart.Name }}
          ...
          ports:
            - name: http
              containerPort: 80 #<== Change to 8080
```

</p>
</details>

2. Try to access the application using the command shown in `helm status` / `NOTES`

### Exercise 2
1. Set some `requests` and `limits` as a default in `values.yaml`. You can use `limits.cpu: "1.0" and limits.memory:"512Mi"`. Decide about the right `requests.cpu` and `requests.memory` using `kubectl top pods`.
2. Calling `index.html` is understandably not the best way to facilitate liveness and readyness probes. Luckily, `spring boot actuator` was enabled when building this image. Switch the livenessProbes and readinessProbes from `/` to `/actuator/health/liveness` and `/actuator/health/readiness`.

    2.1 (optional) Instead of hardcoding the liveness and readiness Paths, make those values available for configuration in `values.yaml`
    
    2.2. (optional) Add a startupProbe

### Bonus Exercise 2

1. Make the application accessible by configuring the ingress correctly. (in `custom-values.yaml`)
2. Enable tls with a valid certificate for this ingress

<details><summary>spoiler - solution</summary>
<p>

```yaml
ingress:
  enabled: true
  className: "nginx"
  annotations:
    cert-manager.io/cluster-issuer: letsencrypt
    kubernetes.io/tls-acme: "true"
  hosts:
    - host: changeme!.mt-k8s.de
      paths:
        - path: /
          pathType: Prefix
  tls:
    - secretName: changeme-mt-k8s-eu
      hosts:
         - changeme!.mt-k8s.de
```

</p>
</details>

### Bonus Exercise 3

1. The spring boot application is configured to output the contents of the property 'envvar' at `/envvar`.
2. Decide on a new greeting and pass it to the application using an environment variable called `ENVVAR`. The contents of this environment variable should not be hardcoded.
3. Create a configmap inside your helm chart. Set the `ENVVAR` variable there and  pass it's content to the container.
    ```shell
    envFrom:
        - configMapRef:
                  name: {{ include "spring-demo-chart.fullname" . }}
    ```
    ---
