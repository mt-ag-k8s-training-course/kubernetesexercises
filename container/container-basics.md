## Container Basics
To get a basic idea how to work with containers, we will use a container engine to communicate with the system's container runtime. In our case that's podman.

```bash
podman info
```

`podman info ` provides us a lot of useful system information, eg which container runtime or cgroup driver is beeing used.




### Getting started

To get started we will pull an example image:

```bash
podman pull docker.io/redis
```

And now we list all images.
```bash
podman images -a
```

You will notice, that without further adding a version tag, we pulled the latest image by default. Using the latest tag is often a bad idea and should be avoided if possible. So let's remove the image and pull another one explicitly. 

```bash
podman image rm docker.io/redis
```

Let's confirm the image is gone by listing all images again.

```bash
podman images -a
```

We now pull a specific image tag.
```bash
podman pull docker.io/redis:6.2.6
```

List all images again to see that we now have a specified tag.

### Running a container

To spawn and run a new container we can simply use `podman run`. To get some visual feedback we will use the interacive (-i) and tty (-t) flags. 

```bash
podman run -it docker.io/redis:6.2.6
```

You can end the process with `ctrl-c`

`podman ps -a` will show us the created container and its status. Since we canceled it, it has been terminated.


Let's spawn another container, this time in detached mode (-d).
```bash
podman run -d docker.io/redis:6.2.6
```
  This time the container keeps running in the background. To display only running processes use `podman ps` 
 
  Copy the container ID.
  
  To start and stop the container use.
```bash
podman stop <CONTAINER_ID>
podman start <CONTAINER_ID>
```
(There is a bug in the latest podman version 1.6.2 on Ubuntu 18.04 which denies starting the stopped container)
To remove it use. (-f flag forces removal if process is still running)
  
```bash
podman rm -f <CONTAINER_ID> 
```

### Image layer

After covering some basics it is time to get back to a more advanced topic. Container images are not really single images, much more they are repositories consisting of one or more image layers. To get a better understanding, we will use `skopeo` to inspect our container image.


```bash
skopeo inspect --config docker://docker.io/redis:6.2.6 | jq
```
```json
    "rootfs": {
        "type": "layers",
        "diff_ids": [
            "sha256:2edcec3590a4ec7f40cf0743c15d78fb39d8326bc029073b41ef9727da6c851f",
            "sha256:9b24afeb7c2f21e50a686ead025823cd2c6e9730c013ca77ad5f115c079b57cb",
            "sha256:4b8e2801e0f956a4220c32e2c8b0a590e6f9bd2420ec65453685246b82766ea1",
            "sha256:529cdb636f61e95ab91a62a51526a84fd7314d6aab0d414040796150b4522372",
            "sha256:9975392591f2777d6bf4d9919ad1b2c9afa12f9a9b4d260f45025ec3cc9b18ed",
            "sha256:8e5669d8329116b8444b9bbb1663dda568ede12d3dbcce950199b582f6e94952"
        ]
    }
```
We can see that the image consists of 6 layers. Each layer presenting a base image change. 

https://hub.docker.com/layers/redis/library/redis/6.2.6/images/sha256-226cbafc637cd58cf008bf87ec9d1548ad1b672ef4279433495bdff100cdb883?context=explore

### Exercise

Why does the image contain 6 layers?
Which container runtime did we use in our examples?


### Useful podman commands

|  podman command | description   |
|---|---|
|  `podman info` | Check host information |
|  `podman pull [image](:[tag])` | Copies images from registry onto local machine. Default is the latest tag, otherwise tag must be specified |
|  `podman images (-a)` | List images on local machine. Without `-a` intermediate image layers are filtered out |
|  `podman run (-t/--tty) (-i/--interactive) [image](:[tag])` | Run a process in a new container. Can be made accessible in the terminal with the flags `-it` |
|  `podman run (-t/--tty) (-i/--interactive) [image](:[tag]) [command]` | Run a process in a *new* container with a command (e.g.: `podman run -t test_image echo "hello"`) |
|  `podman exec (-t/--tty) (-i/--interactive) [container] [command]` | Run a command in a *running* container   (e.g.: `podman exec -it testcontainer /bin/sh`) |
|  `podman run (-d/--detach) (-p/--publish [ip]:[port]) (--name [name]) [image](:[tag])` | Run a detached new container with an open port and a specific name |
|  `podman build (-t/--tag [image name](:[tag])) [docker-/containerfile path]` |  Builds an image from a Dockerfile or Containerfile |
|  `podman stop [container]` |   Stops a running container |
|  `podman start [container]` |   Starts an existing container |
|  `podman rm (-f/--force) [container]` |  Removes a container from the host. `-f` is necessary if the container is running or unusable |
|  `podman rmi (-f/--force) [image(:[tag])]` |  Removes an image from the host. The `-f` will remove all containers that use that image |
|  `podman rmi (-f/--force) --all` |  Removes *all* images from the host. The `-f` will remove all containers that use these images |
|  `podman tag [image(:[tag])] [new name(:[tag])]` | Adds a new name (and tag if specified) to an image  |
|  `podman login [adress] -u [username] -p [password]` | Logs into a registry |
|  `podman logout [adress]` | Logs out of a registry |
|  `podman push [image(:[tag])]` | Deploys an image (latest if not a specified tag) to the registry |
|  `trivy image [image(:[tag])]` | Scans an image for vulnerabilities |
|  `skopeo inspect [image(:[tag])] (\| jq)` | Inspect layers of an image |
