# Helm (1/2) Using helm charts

Navigate to the folder `kubernetesexercises/helm`.

In this exercise, we will practice working with helm charts from remote repositories.

## Helm Chart Repositories
To get started with helm, you first need to add a chart repository:
```bash
helm repo add bitnami https://charts.bitnami.com/bitnami
```

First let's verify that we installed the `bitnami` repository correctly:
```bash
helm repo list       
```

It is important to keep your repository information updated:
```bash
helm repo update
```

To view the charts available in the repository `bitnami`, type:
```bash
helm search repo bitnami
```

## Exploring a helm chart

As an example, we will further inspect the `nginx` chart from the `bitnami` repository we added before. Take a look at the charts metadata...

```bash
helm show chart bitnami/nginx 
```

...the readme...
```bash
helm show readme bitnami/nginx
```

...and the default values:
```bash
helm show values bitnami/nginx | more
```
**Note**: 
You can exit `more` by typing `:q`

There are lots of configuration options. This is quite common, but most of those won't be of interest.

---

To take a deeper look at a charts content, you can download and unpack it with:
```bash
helm pull bitnami/nginx --untar --version 15.14.2
```

Now, inspect the newly created `nginx` directory with the `tree` command from Linux:

```bash
tree nginx
```

The template files (they are called "manifests") that will be filled with values when we install the chart can be found in `nginx/templates`.

## Installing the chart

Before deploying a release, rendering its manifests can make understanding a helm chart much easier:

```bash
helm template ./nginx | more
```

You may notice that only two of the many templates where actually rendered: 
a deployment and a service. Can you imagine why?

<details><summary>answer - click here</summary>
<p>
Some components, like the ingress, are disabled by default in this chart. You can enable them by changing the respective values.
</p>
</details>

---

Now, let's use the values from `~/kubernetesexercises/helm/values-dev.yaml` to customize the release. You can do so with:
```bash
helm upgrade --install weather -f values-dev.yaml bitnami/nginx --version 15.14.2
```

You can view the chart's status with:
```bash
helm ls
```

Installing the chart actually deployed k8s objects for you. You can inspect them with:
```bash
kubectl get all
```

## Updating the release

Our release runs with a service of type ClusterIP. Therefore, we can not access it outside of our cluster, yet. Change the `serviceType` to `LoadBalancer` in the `values-dev.yaml` to make this work. Then upgrade your release

```bash
helm upgrade --install weather bitnami/nginx -f values-dev.yaml --version 15.14.2
```
You may now visit your application using it's external IP address and find out why we called it "weather" (:

## Exercise 1

Next, we will try setting another value. This time, we use the `--set` command which has ultimate precedence:
```bash
helm upgrade --install weather bitnami/nginx -f values-dev.yaml --set image.tag=16.2.2 --version 15.14.2
```

Let's check out our release:
```bash
helm ls
```

The chart seems to be deployed. But does it really work? Let's check more thoroughly with:
```bash
kubectl get all
```

1) Can you determine the error using `kubectl describe`, `kubectl get events` or `kubectl get pod -o yaml`?

<details><summary>spoiler - solution</summary>
<p>

```bash
The image nginx:16.2.2 does not exist! (And the cake is a lie)
```

</p>
</details>

2) Use the commands `helm history weather` and `helm rollback weather <REVISION>` to undo the change. You can find the latest revision id with `helm history weather`

## Bonus Exercise

1) Configure the chart to use an ingress.
2) Configure the ingress to use TLS:

<details><summary>spoiler - solution</summary>
<p>

```yaml
ingress:
  enabled: true
  pathType: Prefix
  tls: true
  hostname: changeme!.mt-k8s.de
  path: /
  annotations:
    kubernetes.io/ingress.class: nginx
    cert-manager.io/cluster-issuer: letsencrypt
    kubernetes.io/tls-acme: "true"
```

</p>
</details>

## Appendix: Useful helm commands for creating and debugging helm charts

|  helm command | description   |
|---|---|
|  `helm repo add [repository-name] [url]` | Add a repository from the internet |
|  `helm repo update` | Update all repositories |
|  `helm upgrade [release] [chart] --install (-n/--namespace [namespace])` | Upgrade a release. If it does not exist on the system, install it |
|  `helm pull (--untar) [chart]` | Download and optionally unpack a helm chart |
|  `helm lint [chart] ` |  Run tests to examine a chart and identify possible issues |
|  `helm show all [chart]` |   Inspect a chart and list its default values |
|  `helm show readme [chart]` |  Show a chart's readme |
|  `helm show values [chart] ` | Display the chart’s values |
|  `helm template [release] [chart] (-f/--values [values-file]) (-s/--show-only [template file]` | Render one or all manifests of a helm chart (e.g.: `helm template test . -f values-edited.yaml -s templates/ingress.yaml`)  |
|  `helm get manifest [release]` |  display the manifest for a named release currently deployed |
|  `helm get values [release] ` |   display the values used to deploy a release currently deployed |
