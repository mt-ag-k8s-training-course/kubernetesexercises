## `kubectl` 101
Kubectl is by far the most important tool when interacting with Kubernetes Clusters. 
It is written in Go and can be extended with a huge collection of plugins.

All the information needed to authenticate with a cluster is managed inside the `kubeconfig` file. It contains information 
about users, namespaces, clusters and authentication mechanism. The `kubeconfig` file represents a sort of local state of `kubectl`.

---
**Note**: A `kubeconfig` can contain information about multiple clusters represented as _contexts_.

---

### View the kubeconfig

Your kubeconfig is usually located in `~/.kube/config` or set using the environment variable `KUBECONFIG`.

Let's start by viewing your kubeconfig:
```bash
kubectl config view
```

First check if you can connect to the cluster:

```bash
kubectl cluster-info
```

### Looking at the nodes

Since we haven't learned about much else, let's start by looking at the cluster's nodes.

```bash
kubectl get nodes
```

Adding `-o wide` also shows the nodes ip addresses
```bash
kubectl get nodes -o wide
```
---

**Note**: On preconfigured kubernetes clusters like `aks`,`gke` or `aws`, only the worker nodes will be visible and 
accessible.

---

Show node memory usage with:

```bash
kubectl top nodes  
```
